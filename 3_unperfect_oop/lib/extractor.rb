class Extractor

  def self.extract(number)
    (2..number).select do |candidate|
      is_prime_number = true
      (2..candidate-1).each do |divisor|
        if(candidate % divisor == 0)
          is_prime_number = false
          break
        end
      end
      is_prime_number
    end
  end
end

package Multi;

sub new {
    my ($class, $a, $b) = @_;
    return bless $class {
        a => $a,
        b => $b,
    };
}

sub get_a {
    my $self = shift;
    return $self->{a};
}


sub get_b {
    my $self = shift;
    return $self->{b};
}

1;

package Single;

my $a;
my $b;

sub new {
    my ($class, $arg_a, $arg_b) = @_;
    $a = $arg_a;
    $b = $arg_b;
}

sub get_a {
    return $a;
}

sub get_b {
    return $b;
}

1;

require 'prime_number'

class Extractor

  def self.extract(number)
    (2..number).select do |candidate|
      PrimeNumber.prime_number?(candidate)
    end
  end
end

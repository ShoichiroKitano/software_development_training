require 'extractor'

result = Extractor.extract(0)
puts '0 ok' if result.empty?

result = Extractor.extract(1)
puts '1 ok' if result.empty?

result = Extractor.extract(2)
puts '2 ok' if result == [2]

result = Extractor.extract(10)
puts '10 ok' if result == [2, 3, 5, 7]

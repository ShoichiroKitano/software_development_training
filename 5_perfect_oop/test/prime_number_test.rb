require 'prime_number'

puts '2 ok' if PrimeNumber.prime_number?(2)
puts '4 ok' unless PrimeNumber.prime_number?(4)
puts '5 ok' if PrimeNumber.prime_number?(5)

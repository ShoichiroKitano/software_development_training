puts '数字を入力しろ愚民ども！'
number = STDIN.gets.to_i

prime_numbers = (2..number).select do |candidate|
                  is_prime_number = true
                  (2..candidate-1).each do |divisor|
                    if(candidate % divisor == 0)
                      is_prime_number = false
                      break
                    end
                  end
                  is_prime_number
                end

puts '素数はこれだ愚民ども！'
puts prime_numbers

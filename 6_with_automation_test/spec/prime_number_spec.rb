require 'prime_number'

describe PrimeNumber do

  it { expect(described_class.prime_number?(2)).to be_truthy }

  it { expect(described_class.prime_number?(4)).to be_falsy }

  it { expect(described_class.prime_number?(5)).to be_truthy }
end

require 'extractor'

describe Extractor do

  it { expect(described_class.extract(0)).to be_empty }

  it { expect(described_class.extract(1)).to be_empty }

  it { expect(described_class.extract(2)).to eq([2]) }

  it { expect(described_class.extract(10)).to eq([2, 3, 5, 7]) }
end

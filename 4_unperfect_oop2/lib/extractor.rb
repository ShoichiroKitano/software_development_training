class Extractor

  def self.extract(number)
    (2..number).select do |candidate|
      prime_number?(candidate)
    end
  end

  def self.prime_number?(number)
    (2..number-1).each do |divisor|
      return false if(number % divisor == 0)
    end
    true
  end
end

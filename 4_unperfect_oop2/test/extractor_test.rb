require 'extractor'

puts 'test prime_number?'

puts '2 ok' if Extractor.prime_number?(2)
puts '4 ok' unless Extractor.prime_number?(4)
puts '5 ok' if Extractor.prime_number?(5)


puts 'test extract'

result = Extractor.extract(0)
puts '0 ok' if result.empty?

result = Extractor.extract(1)
puts '1 ok' if result.empty?

result = Extractor.extract(2)
puts '2 ok' if result == [2]

result = Extractor.extract(10)
puts '10 ok' if result == [2, 3, 5, 7]
